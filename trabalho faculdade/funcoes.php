<?php

function soNumeros($texto)  
{
    return preg_replace("/\D/","", $texto);  
}

function removerCaracteresEspeciais($string) {
    $string = preg_replace(
        array(
            "/[áàãâä]/u", "/[ÁÀÃÂÄ]/u",
            "/[éèêë]/u", "/[ÉÈÊË]/u",
            "/[íìîï]/u", "/[ÍÌÎÏ]/u",
            "/[óòõôö]/u", "/[ÓÒÕÔÖ]/u",
            "/[úùûü]/u", "/[ÚÙÛÜ]/u",
            "/[ñ]/u", "/[Ñ]/u"
        ),
        explode(" ", "a A e E i I o O u U n N"),
        $string
    );

    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);

    return $string;
}