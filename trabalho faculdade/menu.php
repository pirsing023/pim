<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>
        body {
            background-color: #f5f5f5;
        }

        .navbar {
            background-color: rgba(0, 0, 0, 0.1);
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-lg">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="../index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/trabalho faculdade/cadastroFuncionario/cadastroFuncionario.php">Cadastro de funcionarios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-disabled="true" href="/trabalho faculdade/calculoFolha/calculoFolha.php">Calculo de folha de pagamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-disabled="true" href="/trabalho faculdade/contraCheck/contraCheck.php">Demonstrativo de pagamento</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
    <script src="/trabalho faculdade/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>
