<?php include('../menu.php'); ?>
<link href="./cadastroFuncionario.css" rel="stylesheet">

<form id="form">
    <h2>Dados cadastrais</h2>
    
    <div class="row mb-3">
        <label for="empresa" class="col-sm-2 col-form-label">Empresa <b>*</b></label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="empresa">
        </div>
    </div>
    <div class="row mb-3">
        <label for="matricula" class="col-sm-2 col-form-label">Matricula do funcionario <b>*</b></label>
        
        <div  id="matriculaDiv" class="col-sm-10">
            <input type="number" class="form-control" id="matricula">
            &nbsp;
            &nbsp;
            &nbsp;
            <button type="button" id="gerarMatricula" onclick="numeroMatricula()" class="btn btn-primary">Gerar Matricula</button>
        </div>
    </div>
    <div class="row mb-3">
        <label for="cargo" class="col-sm-2 col-form-label">cargo <b>*</b></label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="cargo">
        </div>
    </div>
    <br><br>
    <h2>Dados Pessoais</h2>
    <br><br>
    <div class="row mb-3">
        <label for="nome" class="col-sm-2 col-form-label">Nome <b>*</b></label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="nome">
        </div>
    </div>
    <div class="row mb-3">
        <label for="estado" class="col-sm-2 col-form-label">Estado <b>*</b></label>
        <div class="col-sm-10">
            <select class="form-select" id="estado" aria-label="Floating label select example">
                <option selected>Selecione um estado</option>
            </select>
        </div>    
    </div>
    <div class="row mb-3">
        <label for="cidade" class="col-sm-2 col-form-label">Cidade <b>*</b></label>
        <div class="col-sm-10">
            <select class="form-select" id="cidade" aria-label="Floating label select example">
                <option selected>Selecione uma cidade</option>
            </select>
        </div>    
    </div>
    <div class="row mb-3">
        <label for="endereco" class="col-sm-2 col-form-label">Endereço <b>*</b></label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="endereco">
        </div>
    </div>
    <div class="row mb-3">
        <label for="bairro" class="col-sm-2 col-form-label">Bairro <b>*</b></label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="bairro">
        </div>
    </div>
    <br>
    <button type="button" id="salvar" class="btn btn-primary">Salvar</button>

</form>

<script src="./cadastroFuncionario.js"></script>