<?php include('../menu.php'); ?>
<link href="./contraCheck.css" rel="stylesheet">

<form id="form">
    <h2>Demonstrativo de pagamento</h2>
    
    <div class="row mb-3">
        <label for="funcionarioCheck" class="col-sm-2 col-form-label">Funcionario</label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="funcionarioCheck">
        </div>
    </div>
    <div class="row mb-3">
        <label for="horasTrabalho" class="col-sm-2 col-form-label">Horas de trabalho</label>
        
        <div class="col-sm-10">
            <input type="time" class="form-control" id="horasTrabalho">
        </div>
    </div>
    <div class="row mb-3">
        <label for="faltas" class="col-sm-2 col-form-label">Faltas</label>
        
        <div class="col-sm-10">
            <input type="number" class="form-control" id="faltas">
        </div>
    </div>
    <div class="row mb-3">
        <label for="desconto" class="col-sm-2 col-form-label">Desconto</label>
        
        <div class="col-sm-10">
            <input type="number" class="form-control" id="desconto">
        </div>
    </div>
    <div class="row mb-3">
        <label for="licencasMedicas" class="col-sm-2 col-form-label">Licenças Medicas</label>
        
        <div class="col-sm-10">
            <input type="number" class="form-control" id="licencasMedicas">
        </div>
    </div>
    <div class="row mb-3">
        <label for="beneficios" class="col-sm-2 col-form-label">Beneficios</label>
        
        <div class="col-sm-10">
            <input type="text" class="form-control" id="beneficios">
        </div>
    </div>
    <div class="row mb-3">
        <label for="diaPagamento" class="col-sm-2 col-form-label">Dia de pagamento</label>
        
        <div class="col-sm-10">
            <input type="date" class="form-control" id="diaPagamento">
        </div>
    </div>
    <div class="row mb-3">
        <label for="salarioBruto" class="col-sm-2 col-form-label">Salario Bruto</label>
        
        <div class="col-sm-10">
            <input type="number" class="form-control" id="salarioBruto">
        </div>
    </div>
    <div class="row mb-3">
        <label for="salarioLiquido" class="col-sm-2 col-form-label">Salario liquído</label>
        
        <div class="col-sm-10">
            <input type="number" class="form-control" id="salarioLiquido">
        </div>
    </div>
</form>