CREATE TABLE funcionario (
    id_funcionario integer PRIMARY KEY,
	empresa  varchar(255) not null,
    matricula int not null, 
    cargo varchar(255) not null,
    estado varchar(255) NOT null,
    cidade varchar(255) NOT null,
    endereco varchar(255) NOT null,
    bairro varchar(255) NOT null
)

alter TABLE funcionario add nome varchar(255) not null;
ALTER TABLE funcionario MODIFY id_funcionario INT AUTO_INCREMENT;
