<?php

class Database {
    private $servername = "127.0.0.1";
    private $username = "root";
    private $password = "";
    private $dbname = "trabalho";
    private $conn;

    public function __construct() {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Conexão falhou: " . $e->getMessage();
        }
    }

    public function closeConnection() {
        $this->conn = null;
    }
    
    public function executeQuery($sql) {
        try {
            $success = $this->conn->exec($sql);
    
            if ($success !== false) {
                echo "Inserção bem-sucedida.";
                return true;
            } else {
                echo "Inserção falhou.";
                return false;
            }
        } catch (PDOException $e) {
            echo "Erro na consulta: " . $e->getMessage();
            return false;
        }
    }
    
}

?>
