<?php
require_once '../model/cadastroFuncionario.class.php';
require_once '../filtersEvalidacoes/cadastroFuncionarioFilterEvalidate.php';

class cadastroFuncionario
{
    private $filtrarEvalidar;
    private $funcionarioBanco;

    public function __construct() {
        $this->filtrarEvalidar = new CadastroFuncionarioFilterEvalidate;
        $this->funcionarioBanco = new cadastroFuncionarioModel();

    }

    public function salvar() 
    {    
        try {
            $dadosFiltrados = $this->filtrarEvalidar->filtrar($_POST);
            $this->filtrarEvalidar->validar($dadosFiltrados);
        
            $insercaoSucesso = $this->funcionarioBanco->insert($dadosFiltrados);
        
            if ($insercaoSucesso) {
                echo "Inserção bem-sucedida.";
            } else {
                throw new Exception("Inserção falhou.", 1);
            }
            
        } catch (\Throwable $th) {
            header('Content-Type: text/plain');
            echo $th->getMessage();
            exit();
        }
    }
}

$cadastroFuncionario = new cadastroFuncionario();

if ($_GET['acao'] === 'salvar') {
    $cadastroFuncionario->salvar();
}