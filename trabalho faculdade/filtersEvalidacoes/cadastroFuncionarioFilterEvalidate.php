<?php
include '../funcoes.php';

class CadastroFuncionarioFilterEvalidate
{
    public function filtrar(array $dados): array 
    {
        $dados["empresa"] = removerCaracteresEspeciais($dados["empresa"]);
        $dados["cargo"] = removerCaracteresEspeciais($dados["cargo"]);
        $dados["matricula"] = soNumeros($dados["matricula"]);

        $dados["nome"] = removerCaracteresEspeciais($dados["nome"]);
        $dados["endereco"] = removerCaracteresEspeciais($dados["endereco"]);
        $dados["bairro"] = removerCaracteresEspeciais($dados["bairro"]);
        $dados["cidade"] = removerCaracteresEspeciais($dados["cidade"]);
        $dados["estado"] = removerCaracteresEspeciais($dados["estado"]);

        return $dados;
    }

    public function validar(array $dados) 
    {
        if (strlen($dados['empresa']) <= 5) {
            throw new Exception('O compo empresa deve ter mais de 5 caracteres.');
        }

        if (!isset($dados['matricula'])) {
            throw new Exception('voce deve gerar um numero de matricula');
        }
        
        if (empty($dados["nome"])) {
            throw new Exception('O compo nome e obrigatorio.');
        }
    
        if (empty($dados["estado"])) {
            throw new Exception('O compo estado deve ter ser preenchido.');
        }
    
        if (empty($dados["cidade"])) {
            throw new Exception('O compo cidade deve ter ser preenchido.');
        }
    
        if (empty($dados["endereco"])) {
            throw new Exception('O compo endereco deve ter ser preenchido.');
        }
    
        if (empty($dados["bairro"])) {
            throw new Exception('O compo bairro deve ter ser preenchido.');
        }

        return true;

    }
}
?>
